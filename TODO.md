# TODOs
In vague order of importance:

- Refactor wikimap to separate concerns and for clarity
- Improve internationalized text
  - Proper RTL support for arabic
- Improve wikimap mobile UX
- Fix mod_tile antimeridian clipping
- Add unit tests to wikiscan
- Make the wikiscan local-density sql more performant
- Make wikiscan more performant
- Integrate with Gitlab CI/CD
- Preserve the category hierarchy without overloading the user with irrelevant
  parents
- Investigate importing wikipedia SQL dumps directly to postgis and replacing
  wikiscan/mediawiki/sql processing with a crazy sql query
- Frontend indication of when data was last updated
- Add more languages
