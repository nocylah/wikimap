#!/bin/bash

# setup config files
setup.sh

# start renderd
renderd -f &

while [ ! -S /var/run/renderd/renderd.sock ]; do sleep 1; done

# start mod_tile
apache2ctl -D FOREGROUND &

wikicodes=($WIKICODES)

while true
do
    # preemptively render tiles
    for wikicode in "${wikicodes[@]}"
    do
        # remove existing cache
        rm -rf "/var/cache/mod_tile/${wikicode}"

        render_list -a -t /var/cache/mod_tile -s /var/run/renderd/renderd.sock -Z 10 -m $wikicode
    done

    # Clear cloudflare cache
    if [[ ! -z $CLOUDFLARE_TOKEN && ! -z $CLOUDFLARE_ZONE ]]
    then
        curl -X POST "https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE}/purge_cache" \
             -H "Authorization: Bearer ${CLOUDFLARE_TOKEN}" \
             -H "Content-Type: application/json" \
             --data '{"purge_everything":true}'
    fi

    # Listen for a connection from the wikiscan container, which indicates that
    # a data import has just finished and re-rendering may now begin
    nc -l -N -p 5555
done
