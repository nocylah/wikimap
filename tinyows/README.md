# TinyOWS

TinyOWS handles feature queries from Wikimap.

# Building

```bash
podman build . -t KUBE_PREFIX/tinyows
```
